package com.example.streams.kakfademo;

import java.nio.charset.Charset;
import java.time.Duration;
import java.util.Arrays;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.TimestampExtractor;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Log4j2
public class KakfaDemoApplication {
  public static void main(String[] args) {
    SpringApplication.run(KakfaDemoApplication.class, args);
  }

  @Bean
  public Consumer<KStream<Long, String>> firstConsumer() {

    return input1 ->
        input1.foreach(
            (key, value) -> {
              log.info(
                  "In consumer() - applicationId: kafka-demo-1 - Key: {} value: {}", key, value);
            });
  }

  @Bean
  public Consumer<KStream<Long, String>> secondConsumer() {

    return input ->
        input.process(
            () ->
                new Processor<>() {
                  private ProcessorContext processorContext;

                  @Override
                  public void init(ProcessorContext processorContext) {
                    this.processorContext = processorContext;
                  }

                  @Override
                  public void process(Long key, String value) {
                    log.info(
                        "In secondConsumer() - applicationId: kafka-demo-2 - Key: {} value: {}",
                        key,
                        value);
                    Arrays.stream(processorContext.headers().toArray())
                        .parallel()
                        .forEach(
                            h ->
                                log.info(
                                    "Header - Name: {} Value: {}",
                                    h.key(),
                                    StringUtils.toEncodedString(
                                        h.value(), Charset.defaultCharset())));
                  }

                  @Override
                  public void close() {}
                });
  }

  @Bean
  public BiFunction<KStream<Long, String>, KStream<Long, String>, KStream<Long, String>>
      consumeAndProduce() {
    return (leftSource, rightSource) ->
        leftSource
            .join(
                rightSource,
                (leftValue, rightValue) -> "left=" + leftValue + ", right=" + rightValue,
                JoinWindows.of(Duration.ofMinutes(5)))
            .transform(
                () ->
                    new Transformer<Long, String, KeyValue<Long, String>>() {
                      private ProcessorContext processorContext;

                      @Override
                      public void init(ProcessorContext processorContext) {
                        this.processorContext = processorContext;
                      }

                      @Override
                      public KeyValue<Long, String> transform(Long aLong, String s) {
                        processorContext.headers().add("my-name", "azam".getBytes());
                        processorContext
                            .headers()
                            .add("uid", UUID.randomUUID().toString().getBytes());
                        return KeyValue.pair(aLong, s);
                      }

                      @Override
                      public void close() {}
                    });
  }

  @Bean
  public BiConsumer<KStream<Long, String>, KTable<Long, String>> storeProcessor() {

    return (input1, input2) ->
        input1.foreach(
            (key, value) -> {
              log.info(
                  "In process() - applicationId: kafka-demo-3 - Key: {} value: {}", key, value);
            });
  }

  @Bean
  public TimestampExtractor timestampExtractor() {
    return new WallclockTimestampExtractor();
  }
}
