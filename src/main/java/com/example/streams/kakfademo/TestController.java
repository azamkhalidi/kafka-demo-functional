package com.example.streams.kakfademo;

import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

  @Autowired private InteractiveQueryService interactiveQueryService;

  @GetMapping("/read")
  public ResponseEntity<?> readStateStore() {
    ReadOnlyKeyValueStore<Object, Object> keyValueStore =
        interactiveQueryService.getQueryableStore(
            "demo-store", QueryableStoreTypes.keyValueStore());

    return ResponseEntity.ok(keyValueStore.get(1L));
  }
}
